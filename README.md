# GraphQL Sgac Backend
GraphQL service to handle Complementary Activities for educational institutions.

It's quite simple and easy to learn because it's using a code-generator that helps us to have consitent and standarized Resolvers so users will have an intuitive way to use the service.

You can open issues, send PRs, fork it or anything in order to improve it.

There is not an official stable version but hopefuly I will launch it soon with an online public API and web client.

## Stack
- PostgreSQL
- NodeJS
- TypeScript
- GraphQL [learn more](https://graphql.org/)
- Prisma 2 [learn more](https://www.prisma.io/)
- TypeGraphQL [learn more](https://typegraphql.com/)

## How to run
### You will need a PostgreSQL DB before trying the following steps.env:

1. Set your DB connection string in an `.env` file. Check out the `.env.example` file.
```sh
DATABASE_URL="postgresql://[USER]:[PASSWORD]@[HOST]:[PORT]/[DB_NAME]?schema=public"
```

2. Run Node commands:
```sh
INSTALL DEPENDENCIES
$ yarn install 

DEPLOY CHANGES
$ yarn localDeploy
```

You should see a message like this: 
```sh
🚀 Server ready at: http://localhost:4000
⭐️ See sample queries: http://pris.ly/e/ts/graphql-typegraphql-crud#using-the-graphql-api
```
3. Open your browser and go to `http://localhost:4000` to open the playground.

4. Once you have your app up and running, you can use the start command to avoid deploying changes to the DB:
```sh
$ yarn dev
```

5. Play with the data. You can see the API docs in the right tabs `[DOCS][SCHEMA]`:
```
mutation registerUser {
  registerUser(
    data: {
      name: "John Doe"
      email: "johndoe@mail.com"
      password: "super-secure-password"
    }
  ) {
    id
    role
    profile {
      id
    }
  }
}

query getUsers {
  users {
    id
    name
    email
    password
    role
    studentProfile {
      activities {
        activity {
          id
        }
      }
    }
    notices {
      notice {
        id
      }
    }
    profile {
      id
      activities {
        id
        name
      }
    }
  }
}
```

## Available commands
- `dev`: Will start the app wihout compiling
- `generate`: Will use a typegraph-prisma pluggin to auto-generate ready to use CRUD TypeGraphQL resolvers and type definitions
- `migrate`: Will generate a new SQL file under the migrations folder to securely update the DB to sync with the Prisma Data Model
- `localDeploy`: Will run the Prisma migration, then the code generation, and finally will start the server

## Some key takeaways
- You can define custom resolvers under the `application`layer