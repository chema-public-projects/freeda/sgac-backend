import "reflect-metadata";

import { ApolloServer }  from "apollo-server"
import * as tq from 'type-graphql'
const { ApolloServerPluginLandingPageGraphQLPlayground } = require('apollo-server-core');

import { context } from './context'
import { resolvers as generatedResolvers } from './generated/typegraphql-prisma'
import { domainResolvers } from "./application"


const app = async () => {
  const schema = await tq.buildSchema({
    resolvers: [...generatedResolvers, ...domainResolvers],
  })

  new ApolloServer({
    schema,
    context: context,
    plugins: [ApolloServerPluginLandingPageGraphQLPlayground()]
  })
  .listen({ port: 4000 }, () =>
    console.log(
      `🚀 Server ready at: http://localhost:4000\n⭐️ See sample queries: http://pris.ly/e/ts/graphql-typegraphql-crud#using-the-graphql-api`,
    ),
  )
}

app()
