import * as UserResolvers from './users/resolvers/user-resolver'

export const domainResolvers = [
  ...Object.values(UserResolvers),
]
