import { ObjectType, Field } from "type-graphql";
import { Profile, Role, StudentProfile, User } from "../../../generated/typegraphql-prisma";

@ObjectType({ isAbstract: true })
export class SecureUserResponse implements Partial<User> {
  @Field(_type => String, { nullable: false })
  id!: string;

  @Field(_type => Profile, { nullable: true })
  profile?: Profile | null;

  @Field(_type => StudentProfile, { nullable: false })
  studentProfile?: StudentProfile | null;

  @Field(() => Role, { nullable: true })
  role?: Role;

  @Field(() => String, { nullable: true })
  email!: string;

  @Field(_type => String, { nullable: true })
  name?: string | null;

  @Field({ nullable: true })
  lastName?: string;
}
