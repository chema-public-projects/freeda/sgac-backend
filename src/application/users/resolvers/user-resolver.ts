import * as TypeGraphQL from "type-graphql";
import { Arg, Mutation, Resolver } from "type-graphql";
import { GraphQLResolveInfo } from "graphql";
import bcrypt from "bcrypt";
import {
  User,
  UserWhereUniqueInput,
  Role,
} from "../../../generated/typegraphql-prisma";
import { getPrismaFromContext } from "../../../generated/typegraphql-prisma/helpers";
import { RegisterUserInput, SigninUserInput } from "../inputs";
import { PrismaClient } from '@prisma/client'
import { SecureUserResponse } from "../responses";

@TypeGraphQL.Resolver(_of => User)
export class UserResolver {
  @TypeGraphQL.Mutation(() => User, { nullable: false })
  changeUserRole(
    @TypeGraphQL.Ctx() ctx: any,
    @TypeGraphQL.Info() info: GraphQLResolveInfo,
    @TypeGraphQL.Arg("where") where: UserWhereUniqueInput,
    @TypeGraphQL.Arg("role", () => Role) role: Role,
  ): Promise<User> {
    return getPrismaFromContext(ctx).user.update({ where, data: { role } })
  }

  @TypeGraphQL.Mutation(() => SecureUserResponse, { nullable: false })
  async registerUser(
    @TypeGraphQL.Ctx() ctx: any,
    @TypeGraphQL.Info() _info: GraphQLResolveInfo,
    @TypeGraphQL.Arg("data") data: RegisterUserInput
  ): Promise<SecureUserResponse> {
    const db = getPrismaFromContext(ctx) as PrismaClient;

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(data.password, salt);
    data.password = hashedPassword;

    const user = await db.user.create({
      data: data
    })

    return user as SecureUserResponse
  }

  /**
   * @todo IMPLEMENT A CUSTOM RESPONSE TO RETURN A VALID TOKEN
   * @param ctx 
   * @param _info 
   * @param data 
   * @returns 
   */
  @TypeGraphQL.Query(_type => SecureUserResponse, { nullable: false })
  async signin(
    @TypeGraphQL.Ctx() ctx: any,
    @TypeGraphQL.Info() _info: GraphQLResolveInfo,
    @TypeGraphQL.Arg("data") data: SigninUserInput
  ) {
    const db = getPrismaFromContext(ctx) as PrismaClient;

    const matchedUser = await db.user.findUnique({
      where: { email: data.email }
    });

    const passwordIsValid = bcrypt.compareSync(
      data.password,
      matchedUser?.password || ""
    );

    if (passwordIsValid) {
      return matchedUser;
    }
    
    const loginError = new Error('Wrong user or password');
    loginError.name = 'WRONG_USER_OR_PASSWORD';
    throw loginError;
  }
}
