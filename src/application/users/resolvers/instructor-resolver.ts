import * as TypeGraphQL from "type-graphql";
import { Arg, Mutation, Resolver } from "type-graphql";
import { GraphQLResolveInfo } from "graphql";
import {
  User,
  UserWhereUniqueInput,
  Role,
} from "../../../generated/typegraphql-prisma";
import { getPrismaFromContext } from "../../../generated/typegraphql-prisma/helpers";

@TypeGraphQL.Resolver(_of => User)
export class InstructorResolver {
  @TypeGraphQL.Mutation(() => User, { nullable: false })
  changeUserRole(
    @TypeGraphQL.Ctx() ctx: any,
    @TypeGraphQL.Info() info: GraphQLResolveInfo,
    @TypeGraphQL.Arg("where") where: UserWhereUniqueInput,
    @TypeGraphQL.Arg("role", () => Role) role: Role,
  ): Promise<User> {
    return getPrismaFromContext(ctx).user.update({ where, data: { role } });
  }
}
