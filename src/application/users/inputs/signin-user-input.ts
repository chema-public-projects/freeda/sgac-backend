import { Field, InputType } from "type-graphql";

@InputType({ isAbstract: true })
export class SigninUserInput {
  @Field()
  email!: string;

  @Field()
  password!: string;
}
