import { Field, InputType } from "type-graphql";
import { Role } from '../../../generated/typegraphql-prisma'

@InputType({ isAbstract: true })
export class RegisterUserInput {
  @Field()
  email!: string;

  @Field()
  password!: string;

  @Field({ nullable: true })
  name?: string;

  @Field({ nullable: true })
  lastName?: string;

  @Field(_type => Role, { nullable: true })
  role?: Role;
}
