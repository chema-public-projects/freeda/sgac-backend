-- CreateTable
CREATE TABLE "StudentActivityToNotices" (
    "id" TEXT NOT NULL,
    "studentActivityId" TEXT NOT NULL,
    "noticeId" TEXT NOT NULL,

    PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "StudentActivityToNotices" ADD FOREIGN KEY ("studentActivityId") REFERENCES "StudentActivity"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "StudentActivityToNotices" ADD FOREIGN KEY ("noticeId") REFERENCES "Notice"("id") ON DELETE CASCADE ON UPDATE CASCADE;
